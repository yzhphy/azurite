# Azurite
![Azurite](https://bitbucket.org/yzhphy/azurite/raw/master/Azurite.jpg)
##README 

Version 1.0.0

Azurite (**A ZUR**ich-bred method for finding master
  **I**n**TE**grals) is a *Mathematica*/*Singular* package for finding an integral basis of multi-loop Feynman diagrams. It is written by Alessandro Georgoudis, Kasper J. Larsen and Yang Zhang. 
 
Azurite is a free software released under the GNU General Public License. 
  
  
### To install Azurite

1. Install *Mathematica* 10.0.0 or a more recent version
2. Download and install *Singular* from [https://www.singular.uni-kl.de](https://www.singular.uni-kl.de) .
3. Download Azurite package, version 1.0.0 from [https://bitbucket.org/yzhphy/azurite/raw/master/release/Azurite_1.0.0.tar.gz](https://bitbucket.org/yzhphy/azurite/raw/master/release/Azurite_1.0.0.tar.gz). Extract this archive file.
	* Alternatively, you may also clone the repository by entering this command in your terminal.
	
	```
	git clone git@bitbucket.org:yzhphy/azurite.git
	```
	
4. Create an empty directory on your local computer for temporary files. 

After installation, the main package file is located in the subdirectory ```code```. The examples files and the user manual are located in the subdirectories ```examples``` and ```manual```, respectively.

### Questions

Please write an email to [yang.zhang@phys.ethz.ch](mailto:yang.zhang@phys.ethz.ch) .

